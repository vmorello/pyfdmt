# pyfdmt

A pure python implementation of the Fast DM Transform algorithm as first published in [Zackay & Ofek 2014](https://arxiv.org/abs/1411.5373)


### Citation

If using this module contributes to a scientific publication, please cite the article above, mention the module name and add a footnote with the link to this repository.


### Installation

Clone the repository and type `make install` to install the module in editatble mode with `pip`:

```bash
git clone https://vmorello@bitbucket.org/vmorello/pyfdmt.git
cd pyfdmt/
make install
```

This automatically installs the required dependencies if they are not present.


### Usage

The module provides a `transform()` function that requires the following inputs (see docstring for more details):  

* A two-dimensional input data array in frequency-time order.  
* The frequency of the first channel of the data (first line) in MHz  
* The frequency of the last channel of the data (last line) in MHz  
* The sampling time of the data in seconds  
* The minimum trial dispersion measure  
* The maximum trial dispersion measure  

The output is an `OutputBlock` object that wraps the dedispersed data array, with the following members:  

* `output.data`: the dedispersed data in (DM, time) order. Each line is a dedispersed time series  
* `output.dms`: the list of trial DMs  
* `output.plot()`: plot the data using matplotlib's `imshow()`  
* `output.ymin`: dispersion delay (in samples) that corresponds to the first DM trial  
* `output.ymax`: dispersion delay (in samples) that corresponds to the last DM trial  

Note that the dispersion measure step is fixed by the algorithm, and corresponds to a dispersion delay of one sample across the whole band.

### Notes and Implementation details

**The first frequency channel in the data must be the channel with the highest frequency**. The data are dedispersed with respect to the first / highest frequency channel. If your data comes in the opposite order, flip its lines around.

`pyfdmt` currently handles edge effects at the end of the block by wrapping the dispersion curves around in time. This means that the last `ymax` samples in the output can contain garbage output and should be ignored or discarded.
